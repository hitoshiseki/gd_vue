
const routes = [
  {
    path: '/',
    name: 'governo-digital',
    component: () => import('layouts/GovernoDigital.vue')
  },
  {
    path: '/identificacao',
    name: 'identificacao',
    component: () => import('layouts/Identificacao.vue')
  },
  {
    path: '/autenticacao',
    name: 'autenticacao',
    component: () => import('layouts/Autenticacao.vue')
  },
  {
    path: '/home-gd',
    component: () => import('layouts/MainLayoutGD.vue'),
    children: [
      { path: '', name: 'home-gd', component: () => import('pages/IndexGD.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
