import CustomBreadcrumbs from '../components/breadcrumbs/Breadcrumbs.vue'
import PageTemplate from '../components/pages/PageTemplate.vue'

export default async ({ Vue }) => {
  Vue.component('CustomBreadcrumbs', CustomBreadcrumbs)
  Vue.component('PageTemplate', PageTemplate)
}
