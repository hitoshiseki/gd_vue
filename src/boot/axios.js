import { Loading, QSpinner } from 'quasar'
import axios from 'axios'

export default async ({ Vue, router, store }) => {
  axios.interceptors.request.use(
    config => {
      config.url = `${process.env.API}${process.env.SERVER_CONTEXT}${config.url}`
      Loading.show({
        spinner: QSpinner
      })
      config.headers.Authorization = store.getters['auth/jwt']
      return config
    },
    error => {
      Loading.hide()
      return Promise.reject(error)
    }
  )

  Vue.prototype.$axios = axios
}
