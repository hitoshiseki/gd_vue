import { Notify } from 'quasar'

const configNotify = {
  position: 'top',
  timeout: 2000
}

export const error = (message) => {
  Notify.create({
    color: 'negative',
    message: message,
    ...configNotify
  })
}

export const warning = (message) => {
  Notify.create({
    color: 'warning',
    message: message,
    ...configNotify
  })
}

export const success = (message) => {
  Notify.create({
    color: 'green-5',
    message: message,
    ...configNotify
  })
}
