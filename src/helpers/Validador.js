export const cpfValido = (strCPF) => {
  var soma
  var resto
  soma = 0
  strCPF = strCPF.replace(/[.]/g, '').replace(/[-]/g, '')

  if (strCPF === '00000000000') return false

  for (var i = 1; i <= 9; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i)
  resto = (soma * 10) % 11

  if ((resto === 10) || (resto === 11)) resto = 0
  if (resto !== parseInt(strCPF.substring(9, 10))) return false

  soma = 0
  for (i = 1; i <= 10; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i)
  resto = (soma * 10) % 11

  if ((resto === 10) || (resto === 11)) resto = 0
  if (resto !== parseInt(strCPF.substring(10, 11))) return false
  return true
}

export const cepValido = (strCEP) => {
  strCEP = strCEP.replace(/\D/g, '')
  // Ex: 66060575
  if (strCEP.length === 8) {
    return true
  }
  return false
}

export const emailValido = (email) => {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

export const validar8e30caracteres = (text) => {
  if (text === null || text === undefined || text === '') { return false }
  var re = /^.{8,30}$/
  return re.test(text)
}

export const validarLetraMaiuscula = (text) => {
  if (text === null || text === undefined || text === '') { return false }
  var re = /.*[A-Z]+.*/
  return re.test(text)
}

export const validarLetraMinuscula = (text) => {
  if (text === null || text === undefined || text === '') { return false }
  var re = /.*[a-z]+.*/
  return re.test(text)
}

export const validarCaractereEspecial = (text) => {
  if (text === null || text === undefined || text === '') { return false }
  var re = /.*[!@#$%^&*(),.?":{}|<>]+.*/
  return re.test(text)
}

export const validarSenha = (text) => {
  if (text === null || text === undefined || text === '') { return false }
  var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*(),.?":{}|<>]).{8,30}$/
  return re.test(text)
}
