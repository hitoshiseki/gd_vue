# Governo Digital Vue
Reformulação do frontend do Sistema Governo Digital, utilizando Vue.js com o Framework de UI Quasar.

# Passo-a-passo para executar o projeto

## Passo 1. Clone o projeto para o seu ambiente
```
git clone https://gitlab.com/hitoshiseki/gd_vue.git
```

## Passo 2. Instale o Node.js, de preferência na versão 12.x.x
[https://nodejs.org/dist/latest-v12.x/](https://nodejs.org/dist/latest-v12.x/)

## Passo 3. Vá para o diretório do projeto
```bash
cd diretorio_do_projeto
```

## Passo 4. Instale as dependências com NPM ou Yarn
### Utilizando NPM
Caso não tenha o NPM instalado, [clique aqui para acessar as instruções de instalação](https://www.npmjs.com/get-npm).
Execute o comando abaixo, dentro do diretório do projeto, para instalar as dependências.
```bash
npm install
```

### Ou utilizando YARN
Caso não tenha o yarn instalado, [clique aqui para acessar as instruções de instalação](https://yarnpkg.com/lang/en/docs/install/).
Execute o comando abaixo, dentro do diretório do projeto, para instalar as dependências.
```bash
yarn install
```

## Passo 5. Instale o Quasar CLI
Siga o tutorial disponibilizado em: [https://quasar.dev/quasar-cli/installation](https://quasar.dev/quasar-cli/installation)

## Passo 6. Inicie a aplicação no ambiente de desenvolvimento local (hot-code reloading, error reporting, etc.)
Com o Quasar CLI instalado e dentro do diretório do projeto, execute o comando abaixo:
```bash
quasar dev
```

## Configuração do VS Code
Caso queira configurar o VS Code de acordo com o padrão utilizado no projeto. Siga o tutorial abaixo
[https://quasar.dev/start/vs-code-configuration](https://quasar.dev/start/vs-code-configuration)
